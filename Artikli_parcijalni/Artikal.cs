﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artikli_parcijalni
{
    class Artikal
    {
        // Atributi klase Artikal sa dodatkom Enumeracije Korekcija koji sluzi za prikaz objekata
        //koji su korigovani a koji ne
        private Korekcija korekcija;
        private string sifra;
        private string boja;
        private string velicina;
        private int nabavljenaKolicina;
        private int kolicinaNaLageru;
        private float nabavnaCena;
        private float prodajnaCena;

        /// <summary>
        /// Konstruktor klase Artikal
        /// </summary>
        /// <param name="sifra">string</param>
        /// <param name="boja">string</param>
        /// <param name="velicina">string</param>
        /// <param name="nabavljenaKolicina">int</param>
        /// <param name="kolicinaNaLageru">int</param>
        /// <param name="nabavnaCena">float</param>
        /// <param name="prodajnaCena">float</param>
        public Artikal(string sifra, string boja, string velicina, int nabavljenaKolicina, int kolicinaNaLageru,
            float nabavnaCena, float prodajnaCena)
        {
            this.korekcija = Korekcija.Nepromenjeno;
            this.sifra = sifra;
            this.boja = boja;
            this.velicina = velicina;
            this.nabavljenaKolicina = nabavljenaKolicina;
            this.kolicinaNaLageru = kolicinaNaLageru;
            this.nabavnaCena = nabavnaCena;
            this.prodajnaCena = prodajnaCena;
        }
        /// <summary>
        /// Funkcija koja vrsi promenu prodajne cene u odnosu na kolicinu odredjenog artikla
        /// </summary>
        /// <returns>float</returns>
        public virtual float PromenaCena()
        {
            if (this.kolicinaNaLageru < this.nabavljenaKolicina / 4)
            {
                this.korekcija = Korekcija.Nepromenjeno;
                Console.WriteLine("Korekcija prodajne cene artikla nije bila potrebna");
                return this.prodajnaCena;
            }
            else if (this.kolicinaNaLageru < this.nabavljenaKolicina / 2)
            {
                float novaProdajnacena = (this.nabavnaCena + this.prodajnaCena) / 2;
                this.korekcija = Korekcija.Korigovano;
                Console.WriteLine("Korekcija prodajne cene artikla je izvrsena.");
                return this.prodajnaCena = novaProdajnacena;
            }
            else
            {
                this.prodajnaCena = this.nabavnaCena;
                this.korekcija = Korekcija.Korigovano;
                Console.WriteLine("Korekcija prodajne cene artikla je izvrsena.");
                return this.prodajnaCena;
            }
        }

        
        /// <summary>
        /// Svojstvo koji uzima string iz privatne promenjlive sifra
        /// </summary>
        public string Sifra { get { return this.sifra; } }
        /// <summary>
        /// Svojstvo koje uzima i postavlja(ako je potrebno) vrednost promenljive prodajnaCena
        /// </summary>
        public float ProdajnaCena { get { return this.prodajnaCena; } set { this.prodajnaCena = value; } }
        /// <summary>
        /// Svojstvo koje uzima i postavlja(ako je potrebno) vrednost promenljive korekcija
        /// </summary>
        public Korekcija K { get { return this.korekcija; } set { this.korekcija = value; } }
    }
}
