﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artikli_parcijalni
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Unesite koliko ukupno artikala butik moze da ima: ");
            int.TryParse(Console.ReadLine(), out int broj);
            Butik b = new Butik(broj);
            //b.nizArtikala[0] = new Artikal("ja", "plava", "XL", 60, 14, 56.52f, 75.23f);
            //b.nizArtikala[1] = new ArtikalSaGreskom("ti", "crvena", "XXL", 54, 35, 56.52f, 75.23f, 34.32f);
            //b.nizArtikala[2] = new Artikal("on", "zelena", "XXXL", 120, 59, 56.52f, 75.23f);
            //b.nizArtikala[3] = new ArtikalSaGreskom("ona", "zuta", "L", 45, 23, 56.52f, 75.23f, 34.32f);
            //b.nizArtikala[4] = new Artikal("ono", "crna", "M", 54, 35, 56.52f, 75.23f);
            b.UnosArtikala();
            Console.WriteLine();
            Console.WriteLine("Nakon unosa artikala!!");
            Console.WriteLine();
            b.PrikazPreKorekcije();
            b.KorekcijaCena();
            Console.WriteLine();
            Console.WriteLine("Nakon korekcije cena!!");
            Console.WriteLine();
            b.PrikazPosleKorekcije();
        }
    }
}
