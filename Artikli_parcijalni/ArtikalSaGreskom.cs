﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artikli_parcijalni
{
    /// <summary>
    /// Kreiranje izvedene klase ArtikalSaGreskom
    /// </summary>
    class ArtikalSaGreskom : Artikal
    {
        /// <summary>
        /// Dodatni atribut izvedene klase
        /// </summary>
        private float procenatGreske;
        /// <summary>
        /// Konstruktor izvedene klase
        /// </summary>
        /// <param name="sifra">string</param>
        /// <param name="boja">string</param>
        /// <param name="velicina">string</param>
        /// <param name="nabavljenaKolicina">int</param>
        /// <param name="kolicinaNaLageru">int</param>
        /// <param name="nabavnaCena">float</param>
        /// <param name="prodajnaCena">float</param>
        /// <param name="procenatGreske">float</param>
        public ArtikalSaGreskom(string sifra, string boja, string velicina, int nabavljenaKolicina, int kolicinaNaLageru,
            float nabavnaCena, float prodajnaCena, float procenatGreske)
            : base(sifra, boja, velicina, nabavljenaKolicina, kolicinaNaLageru, nabavnaCena, prodajnaCena)
        {
            this.procenatGreske = procenatGreske;
        }
        /// <summary>
        /// Funkcija koja vrsi dodatnu korekciju ako postoji artikal sa greskom
        /// </summary>
        /// <returns>float</returns>
        public override float PromenaCena()
        {
            base.ProdajnaCena = base.PromenaCena() * (1 - (this.procenatGreske / 100));
            base.K = Korekcija.ASG;
            Console.WriteLine("Korekcija prodajne cene artikla sa greskom je izvrsena!");
            return base.ProdajnaCena;
        }
    }
}
