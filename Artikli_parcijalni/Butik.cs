﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Artikli_parcijalni
{
    /// <summary>
    /// Kreiranje klase butik
    /// </summary>
    class Butik
    {
        /// <summary>
        /// Privatni atributi klase
        /// </summary>
        private int brojArtikala;
        public Artikal[] nizArtikala;
        /// <summary>
        /// Konstruktor klase Butik
        /// </summary>
        /// <param name="broj">int</param>
        public Butik(int broj)
        {
            this.brojArtikala = 0;
            this.nizArtikala = new Artikal[broj];
        }
        /// <summary>
        /// Staticna funkcija koja sluzi samo za unos osnovnih podataka bilo za objekat klase Artikal
        /// ili ArtikalSaGreskom
        /// </summary>
        /// <param name="sifra">string</param>
        /// <param name="boja">string</param>
        /// <param name="velicina">string</param>
        /// <param name="nabavljenaKolicina">int</param>
        /// <param name="kolicinaNaLageru">int</param>
        /// <param name="nabavnaCena">float</param>
        /// <param name="prodajnaCena">float</param>
        private static void Podaci(out string sifra,out string boja,out string velicina,out int nabavljenaKolicina,
            out int kolicinaNaLageru, out float nabavnaCena, out float prodajnaCena)
        {
            Console.WriteLine("Sifra artikla: ");
            sifra = Console.ReadLine();
            Console.WriteLine("Boja artikla: ");
            boja = Console.ReadLine();
            Console.WriteLine("Velicina artikla: ");
            velicina = Console.ReadLine();
            Console.WriteLine("Nabavljena kolicina: ");
            int.TryParse(Console.ReadLine(), out nabavljenaKolicina);
            Console.WriteLine("Kolicina na lageru: ");
            int.TryParse(Console.ReadLine(), out kolicinaNaLageru);
            Console.WriteLine("Nabavna cena: ");
            float.TryParse(Console.ReadLine(), out nabavnaCena);
            Console.WriteLine("Prodajna cena: ");
            float.TryParse(Console.ReadLine(), out prodajnaCena);
        }
        /// <summary>
        /// Funkcija koja vrsi kreiranje objekata i implementira funkciju Podaci()
        /// </summary>
        public void UnosArtikala()
        {
            for (int i = 0; i < nizArtikala.Length; i++)
            {
                Console.WriteLine("Unos novog artikla!");
                Console.WriteLine("Opcije: 1 - Artikl bez greske; 2 - Artikl sa greskom");
                int.TryParse(Console.ReadLine(), out int opcija);
                while (opcija < 1 || opcija > 2)
                {
                    Console.WriteLine("Opcije: 1 - Artikl bez greske; 2 - Artikl sa greskom");
                    int.TryParse(Console.ReadLine(), out opcija);
                }
                switch (opcija)
                {
                    case 1:
                        Podaci(out string sifra, out string boja, out string velicina, out int nabavljenaKolicina,
                                out int kolicinaNaLageru, out float nabavnaCena, out float prodajnaCena);
                        nizArtikala[i] = new Artikal(sifra,boja,velicina,nabavljenaKolicina,kolicinaNaLageru, nabavnaCena, prodajnaCena);
                        break;
                    case 2:
                        Podaci(out sifra, out boja, out velicina, out nabavljenaKolicina,
                                out kolicinaNaLageru, out nabavnaCena, out prodajnaCena);
                        Console.WriteLine("Procenat greske (0 - 100)");
                        float.TryParse(Console.ReadLine(), out float procenatGreske);
                        nizArtikala[i] = new ArtikalSaGreskom(sifra, boja, velicina, nabavljenaKolicina, kolicinaNaLageru, nabavnaCena, prodajnaCena, procenatGreske);
                        break;
                }
                this.brojArtikala++;
            }
        }
        /// <summary>
        /// Funkcija koja vrsi korekciju cena u celom nizu
        /// </summary>
        public void KorekcijaCena()
        {
            for(int i = 0; i < nizArtikala.Length; i++)
            {
                Console.WriteLine($"{i+1}. objekat!");
                nizArtikala[i].PromenaCena();
            }
        }
        /// <summary>
        /// Funkcija za prikaz objekata pre izvrsene korekcije cena, tj. nakon unosa objekata u niz
        /// </summary>
        public void PrikazPreKorekcije()
        {
            Console.WriteLine("Stanje:");
            foreach (Artikal a in nizArtikala)
            {
                Console.Write($"Sifra artikla: {a.Sifra} Prodajna cena: {a.ProdajnaCena} Korekcija {a.K}");
                Console.WriteLine();

            }
            Console.WriteLine();
        }
        /// <summary>
        /// Funkcija za prikaz objekata nakon izvrsene korekcije cena.
        /// </summary>
        public void PrikazPosleKorekcije()
        {
            Console.WriteLine("Stanje:");
            foreach(Artikal a in nizArtikala)
            {
                if(a.K == Korekcija.Korigovano || a.K == Korekcija.ASG)
                {
                    Console.Write($"Sifra artikla: {a.Sifra} Prodajna cena: {a.ProdajnaCena} Korekcija: {a.K}");
                    Console.WriteLine();
                }
                    
                    
            }
        }
    }
}
